# TLS Certificates

This folder contains ansible-vault encrypted TLS certificates which are used for Client Authentication (also known as mTLS).

The server certificates are handled entirely by [Traefik](https://docs.traefik.io), which implements an ACME client and automatically keeps certificates from an ACME CA server up to date. This is configured in the docker-compose file. There's also a variable to switch between using Let's Encrypt's staging CA server (which isn't rate limited) and the production one (which is). If using the staging server, when testing your remote cache you will need to specify the root certificate, as the staging CA's root certificate will almost certainly not be trusted by your distribution.