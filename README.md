# Freedesktop SDK artifact server

This repository caintains the ansible playbook for deploying a [BuildStream artifact server](https://docs.buildstream.build/1.4.3/install_artifacts.html) with valid SSL certificates.

## Ansible Host configuration

Firstly Ansible must know where the host machine exists and how to connect to it, this is done via the *hosts* file in this project.


## SSL certificates

To make our cache server easily accesible we configure a DNS that can be used by any projects to acess the cache.

Currently we use:

        freedesktop-sdk-cache.codethink.co.uk

The certificates are automatically generated via the [Dehydrated client](https://github.com/dehydrated-io/dehydrated)/nginx configuration documented in
the Ansible scripts.

In order to obtain a VALID certificate you must ensure Dehydrated is configured with the official Lets Encrypt
URL, instead of staging(testing) URL, this can be changed via the config file, found under */etc/dehydrated/config.sh*

## Deployment
To deploy the Ansible you can run:

    ansible-playbook -i hosts ./releases/playbook.yml -e 'ansible_python_interpreter=/usr/bin/python3' --vault-password-file 'foo-bar'

This will select the artifacts server from the *hosts* file and then run the steps documented inside *playbook.yml*

When deploying to production for freedesktop-sdk we need ensure that we can decrypt our public/private keys that artifact servers uses authenticate users, to do this you
must pass the ansible-vault password the the *--vault-password-file*, this file is currently controlled by adds68, jjardon, valentind.

## Customising this deployment 
If you would like to adapt this deployment for your own cache server deployment there are a few things you should note.

* Currently this adapted to work only with CentOS, we would like to improve this in the future.
* You would need to modify the *hosts* file to point to your servcers IP address
* You would need to also modify the certifcate section in *playbook.yml* to your own public/private keys
* Finally you would need to change the *{{ssl_url}}* in *playbook.yml*  to point to your own custom domain.
